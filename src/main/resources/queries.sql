drop database IF EXISTS Security;
create database IF NOT EXISTS Security;
use Security;

create table IF NOT EXISTS Resources(
id int NOT NULL AUTO_INCREMENT,
name TEXT,
category TEXT,
PRIMARY KEY (id)
);
create table IF NOT EXISTS Policy (
id int NOT NULL AUTO_INCREMENT,
name varchar(255),
r_id int,
is_recursive int,
is_enabled int,
PRIMARY KEY (id),
FOREIGN KEY (r_id) REFERENCES Resources(id)
);

create table IF NOT EXISTS Conditions(
id int NOT NULL AUTO_INCREMENT,
category TEXT,
p_id int,
perms TEXT,
PRIMARY KEY (id),
FOREIGN KEY (p_id) REFERENCES Policy(id)
);

create table IF NOT EXISTS Roles(
id int NOT NULL AUTO_INCREMENT,
name varchar(255),
PRIMARY KEY (id),
UNIQUE (name)
);

create table IF NOT EXISTS Users(
id int NOT NULL AUTO_INCREMENT,
name varchar(255),
PRIMARY KEY (id),
UNIQUE (name)
);

create table IF NOT EXISTS UserRoles(
u_id int,
r_id int,
PRIMARY KEY (u_id,r_id),
FOREIGN KEY (u_id) REFERENCES Users(id),
FOREIGN KEY (r_id) REFERENCES Roles(id)
);

create table IF NOT EXISTS UserConditions(
u_id int,
c_id int,
PRIMARY KEY (u_id,c_id),
FOREIGN KEY (u_id) REFERENCES Users(id),
FOREIGN KEY (c_id) REFERENCES Conditions(id)
);





create table IF NOT EXISTS Audits(
id int NOT NULL AUTO_INCREMENT,
user TEXT,
access_time long,
access_type TEXT,
response TEXT,
r_id int,
policy_enforcer int,
PRIMARY KEY (id),
FOREIGN KEY (r_id) REFERENCES Resources(id)
);

insert into Roles (name) values('admin');
insert into Roles (name) values('developer');
insert into Roles (name) values('deployment');

insert into Users (name) values('admin');
insert into Users (name) values('usman');
insert into Users (name) values('haseeb');
insert into Users (name) values('junaid');
insert into Users (name) values('adnan');
insert into Users (name) values('baber');
