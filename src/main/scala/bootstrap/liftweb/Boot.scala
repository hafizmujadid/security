package bootstrap.liftweb

import org.apache.log4j.Logger
import net.liftweb.http._
import com.platalytics.security.utils._

/**
 * A class that's instantiated early and run.  It allows the application
 * to modify lift's environment
 */
class Boot extends Bootable {
	private lazy val logger = Logger.getLogger(this.getClass.getName)

	def boot {
		
		Constants.loadConfiguration()
		
		// Binding Service as a Restful API
		LiftRules.statelessDispatchTable.append(WebServices);
		// resolve the trailing slash issue
		LiftRules.statelessRewrite.prepend({
			case RewriteRequest(ParsePath(path, _, _, true), _, _) if path.last == "index" => RewriteResponse(path.init)
		})

		logger.info("Booting webservices...")
		logger.info("Starting job: TestJob...")

	}
}