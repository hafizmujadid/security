package bootstrap.liftweb
import net.liftweb.http.rest.RestHelper
import net.liftweb.http.Req
import net.liftweb.http.S
import net.liftweb.json.JsonAST.JString
import org.apache.log4j.Logger
import net.liftweb.http.PlainTextResponse
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import com.platalytics.security.utils.MongoUtil
import com.platalytics.security.utils.DataHolder
import com.platalytics.security.core.PolicyEvaluator
import com.platalytics.security.auditing.AuditDTO
import java.util.Date
import com.platalytics.security.auditing.AuditHandler
import java.util.Calendar
import com.platalytics.security.utils.Util
object WebServices extends RestHelper {

  serve {
    case Get("api" :: "security" :: "authorize" :: Nil, req) => for {
      resource <- S.param("resource") ?~ "resource parameter not found."
      user <- S.param("user") ?~ "user parameter not found."
      company <- S.param("company") ?~ "company parameter not found."
      action <- S.param("action") ?~ "permission parameter not found."
    } yield PlainTextResponse(evaluate(resource, user, company, action))
  }
  serve {
    case Get("api" :: "security" :: "isallowed" :: Nil, req) => for {
      resource_id <- S.param("resource_id") ?~ "resource_id parameter not found."
      resource_type <- S.param("resource_type") ?~ "resource_type parameter not found."
      user <- S.param("user") ?~ "user parameter not found."
      company <- S.param("company") ?~ "company parameter not found."
      actions <- S.param("action") ?~ "permission parameter not found."
    } yield PlainTextResponse(evaluate(resource_id, resource_type, user, company, actions))
  }
  serve {
    case Get("api" :: "security" :: "evaluateRec" :: Nil, req) => for {
      resource_id <- S.param("resource_id") ?~ "resource_id parameter not found."
      resource_type <- S.param("resource_type") ?~ "resource_type parameter not found."
      user <- S.param("user") ?~ "user parameter not found."
      company <- S.param("company") ?~ "company parameter not found."
      actions <- S.param("action") ?~ "permission parameter not found."
    } yield PlainTextResponse(controlEvaluation(resource_id, resource_type, user, company, actions))
  }

  serve {
    case Get("api" :: "security" :: "update" :: Nil, req) => JString(DataHolder.load("saman.ashraf@platalytics.com"))
  }

  serve {
    case Get("api" :: "security" :: "test" :: Nil, req) => JString("Working")
  }

  serve {
    case Get("api" :: "security" :: "list" :: Nil, req) => for {
      resource <- S.param("resource") ?~ "resource parameter not found."
      user <- S.param("user") ?~ "user parameter not found."
      company <- S.param("company") ?~ "company parameter not found."
    } yield PlainTextResponse(listResource(resource, user, company))
  }

  serve {
    case Get("api" :: "security" :: "audit" :: Nil, req) => for {
      action <- S.param("action") ?~ "resource parameter not found."
      response <- S.param("response") ?~ "user parameter not found."
      user <- S.param("user") ?~ "user parameter not found."
      resourceName <- S.param("resourcename") ?~ "user parameter not found."
      resourceType <- S.param("resourcetype") ?~ "user parameter not found."
    } yield PlainTextResponse(audit(action, response, user, resourceName, resourceType))
  }
  serve {
    case Get("api" :: "security" :: "getAudits" :: Nil, req) => for {
      start <- S.param("start") ?~ "resource parameter not found."
      end <- S.param("limit") ?~ "user parameter not found."
    } yield PlainTextResponse(MongoUtil.list(start.toInt, end.toInt))
  }
  serve {
    case Get("api" :: "security" :: "isadmin" :: Nil, req) => for {
      user <- S.param("user") ?~ "user parameter not found."
      company <- S.param("company") ?~ "company parameter not found."
    } yield PlainTextResponse(isAdmin(user, company))
  }

  //helper methods

  def evaluate(resource: String, user: String, company: String, actions: String): String = {
    DataHolder.load(company)
    val result = PolicyEvaluator.evaluate(user, resource, actions.split(","))
    MongoUtil.close()
    DataHolder.group_roles.empty
    DataHolder.roles.empty
    val jsonObj = Json.toJson(result)
    Json.stringify(jsonObj).replace("[[", "[").replace("},{", ",").replace("],[", ",").replace("]]", "]")
  }

  def evaluate(resource_id: String, resource_type: String, user: String, company: String, actions: String): String = {
    DataHolder.load(company)
    val result = PolicyEvaluator.evaluate(user, resource_id, resource_type, actions.split(","))
    MongoUtil.close()
    DataHolder.group_roles.empty
    DataHolder.roles.empty
    val jsonObj = Json.toJson(result)
    Json.stringify(jsonObj).replace("[[", "[").replace("},{", ",").replace("],[", ",").replace("]]", "]")
  }
  def evaluateRec(resource_id: String, resource_type: String, user: String, company: String, actions: String): String = {
    DataHolder.load(company)
    val result = PolicyEvaluator.evaluateRec(user, resource_id, resource_type, actions.split(","))
    MongoUtil.close()
    DataHolder.group_roles.empty
    DataHolder.roles.empty
    val jsonObj = Json.toJson(result)
    Json.stringify(jsonObj).replace("[[", "[").replace("},{", ",").replace("],[", ",").replace("]]", "]")
  }

  def controlEvaluation(resource_id: String, resource_type: String, user: String, company: String, actions: String): String = {
    if (resource_type.equalsIgnoreCase("project"))
      evaluate(resource_id, resource_type, user, company, actions)
    else
      evaluateRec(resource_id, resource_type, user, company, actions)
  }

  def listResource(resourceType: String, user: String, company: String) = {
    DataHolder.load(company)
    val result = MongoUtil.listResource(resourceType, user)
    MongoUtil.close()
    DataHolder.group_roles.empty
    DataHolder.roles.empty
    val jsonObj = Json.toJson(result)
    Json.stringify(jsonObj).replace("[[", "[").replace("},{", ",").replace("],[", ",").replace("]]", "]")
  }

  def isAdmin(user: String, company: String) = {
    DataHolder.load(company)
    val result = MongoUtil.isAdmin(user)
    MongoUtil.close()
    DataHolder.group_roles.empty
    DataHolder.roles.empty
    val jsonObj = Json.toJson(result)
    Json.stringify(jsonObj).replace("[[", "[").replace("},{", ",").replace("],[", ",").replace("]]", "]")

  }

  def audit(action: String, response: String, user: String, resourceName: String, resourceType: String) = {
    var date = new Date()
    val cal = Calendar.getInstance
    cal.setTime(date)
    var auditObj = new AuditDTO
    auditObj.action = action
    auditObj.datetime = date.getTime()
    auditObj.date = Util.theMonth(cal.get(Calendar.MONTH)) + " " + cal.get(Calendar.DAY_OF_MONTH) + " " + cal.get(Calendar.YEAR)
    auditObj.time = cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND)
    auditObj.resourceName = resourceName
    auditObj.resourceType = resourceType
    auditObj.response = response
    auditObj.user = user
    AuditHandler.audit(auditObj)
    MongoUtil.close()
    DataHolder.group_roles.empty
    DataHolder.roles.empty
    val jsonObj = Json.toJson("audit has been written")
    Json.stringify(jsonObj).replace("[[", "[").replace("},{", ",").replace("],[", ",").replace("]]", "]")

  }
}
