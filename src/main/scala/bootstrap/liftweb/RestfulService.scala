package bootstrap.liftweb

import net.liftweb.http.rest.RestHelper
import net.liftweb.http.Req
import net.liftweb.http.S
import net.liftweb.json.JsonAST.JString
import org.apache.log4j.Logger
import net.liftweb.http.PlainTextResponse
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import com.platalytics.security.core.User

object RestfulService extends RestHelper {

	serve {
		case Get("api" :: Nil, req) => JString("abc")
	}

	serve {
		case Get("api" :: "accesses" :: "evaluation" :: Nil, req) => for {
			resource <- S.param("resource") ?~ "resource parameter not found."
			user <- S.param("user") ?~ "user parameter not found."
			role <- S.param("role") ?~ "role parameter not found."
			actions <- S.param("actions") ?~ "actions parameter not found."
		} yield PlainTextResponse(evaluateAll(resource, user, role, actions, req))
	}
	serve {
		case Get("api" :: "check" :: "permissions" :: Nil, req) => for {
			resource <- S.param("resource") ?~ "resource parameter not found."
			user <- S.param("user") ?~ "user parameter not found."
			role <- S.param("role") ?~ "role parameter not found."
			actions <- S.param("actions") ?~ "actions parameter not found."
		} yield PlainTextResponse(evaluate(resource, user, role, actions, req))
	}

	def evaluateAll(res: String, uname: String, role: String, actions: String, req: Req): String = {
		val service = req.param("service") getOrElse (null)
		val perms = actions.split(",") //actions.split(",")
		var topMap: Map[String, JsValue] = Map()
		for (i <- 0 until perms.length) {
			topMap += (perms(i) -> Json.toJson(true.toString))//Json.toJson(result.toString)
		}
		val jsonObj = Json.toJson(topMap)
		Json.stringify(jsonObj).replace("[[", "[").replace("},{", ",").replace("],[", ",").replace("]]", "]")
	}
	def evaluate(res: String, uname: String, role: String, actions: String, req: Req): String = {
		println("Resource => " + res)
		println("user => " + uname)
		println("role => " + role)
		val user = new User
		user.name = uname.toLowerCase()
		user.role = role.toLowerCase()
		val service = req.param("service") getOrElse (null)
		val perms = actions.split(",") //actions.split(",")
		var topMap: Map[String, JsValue] = Map()
		for (i <- 0 until perms.length) {
			topMap += (perms(i) -> Json.toJson(true.toString))//Json.toJson(result.toString)
		}
		val jsonObj = Json.toJson(topMap)
		Json.stringify(jsonObj).replace("[[", "[").replace("},{", ",").replace("],[", ",").replace("]]", "]")
	}
}
