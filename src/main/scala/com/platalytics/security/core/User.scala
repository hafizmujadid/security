package com.platalytics.security.core

import scala.beans.BeanProperty

class User {
	@BeanProperty var id: String = _
	@BeanProperty var name: String = _
	@BeanProperty var role: String = _
}