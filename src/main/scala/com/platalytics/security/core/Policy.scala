package com.platalytics.security.core

import scala.beans.BeanProperty

class Policy {
	@BeanProperty var id: String = _
	@BeanProperty var r_name: String = _
	@BeanProperty var r_id: String = _
	@BeanProperty var r_type: String = _
	@BeanProperty var groups: Array[String] = _
	@BeanProperty var is_enabled: Boolean = _
}