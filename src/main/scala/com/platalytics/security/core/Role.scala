package com.platalytics.security.core

class Role {
	var _id: String = _
	var title: String = _
	var permissions: Permissions = _
	
	
}
class Permissions{
	var project:Array[String]=_
	var cluster:Array[String]=_
	var sku:Array[String]=_
	var services:Array[String]=_
	var team:Array[String]=_
	var bundle:Array[String]=_
	var payment:Array[String]=_
	var payment_logs:Array[String]=_
	var billing_payment_method:Array[String]=_
}