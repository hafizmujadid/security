package com.platalytics.security.core
import com.platalytics.security.utils.MongoUtil
import com.platalytics.security.utils.DataHolder
import scala.Array.canBuildFrom
import com.platalytics.security.utils.Constants
/*
 This class evaluates policies related to a resource. 
 It has multiple methods to help evaluating policies.
 */
object PolicyEvaluator {
	def evaluate(user: String, resource: String, action: String): Boolean = {
		val groups = MongoUtil.findGroups(user)
		groups.foreach(group => {
			val roles = DataHolder.group_roles.get(group).get._2
			var perms = roles.flatMap(role => DataHolder.roles.get(role).get.get(resource).get).distinct.map(_.toLowerCase())
			if (perms.contains(action.toLowerCase()))
				return true
		})
		return false
	}
	def evaluate(user: String, resource: String, actions: Array[String]): Map[String, Boolean] = {
		var result = Map.empty[String, Boolean]
		val groups = MongoUtil.findGroups(user)

		resource match {
			case Constants.PAYMENT =>
				payementHandler(user, actions, groups)
			case _ =>
				help(user, resource, actions, groups)
		}
	}
	def evaluate(user: String, id: String, resource: String, actions: Array[String]): Map[String, Boolean] = {
		var result = Map.empty[String, Boolean]
		val pgroups = MongoUtil.findGroups(id, resource)
		val ugroups = MongoUtil.findGroups(user)
		val commonGroups = pgroups.intersect(ugroups)

		ugroups.foreach(groupID => {
			if (MongoUtil.isAdmin(user)) {
				actions.foreach(action => {
					if (!result.contains(action))
						result += (action -> true)
				})
				return result
			}
		})
		if (pgroups == null) {
			actions.foreach(action => {
				if (!result.contains(action))
					result += (action -> false)
			})
			return result
		}
		help(user, resource, actions, commonGroups)
	}
	def findPerms(resourceType: String, groups: Array[String]): Array[String] = {
		var perms = new StringBuilder
		groups.foreach(group => {
			val roles = DataHolder.group_roles.get(group).get._2
			perms.append(roles.flatMap(role => DataHolder.roles.get(role).get.get(resourceType).get).distinct.map(_.toLowerCase()).mkString(",") + ",")

		})
		return perms.dropRight(1).split(',')
	}

	private def help(user: String, resourceType: String, actions: Array[String], groups: Array[String]): Map[String, Boolean] = {
		var result = Map.empty[String, Boolean]
		var flag = false
		groups.foreach(groupID => {
			val roles = DataHolder.group_roles.get(groupID).get._2
			var perms = roles.flatMap(roleId => DataHolder.roles.get(roleId).get.get(resourceType).get).distinct.map(_.toLowerCase())
			actions.foreach(action => {
				if (perms.contains(action)) {
					if ((action.equals(Constants.UPDATE) || action.equals(Constants.DELETE)
						|| action.equals(Constants.CREATE) || action.equals(Constants.PAY)
						|| action.equals(Constants.DEPLOY))) {
						flag = true
					}
					result += (action -> true)
				}
			})
		})
		actions.foreach(action => {
			if (!result.contains(action)) {
				if (action.equals(Constants.VIEW)) {
					result += (action -> flag)
				} else
					result += (action -> false)
			}
		})
		result
	}
	private def payementHandler(user: String, actions: Array[String], groups: Array[String]): Map[String, Boolean] = {
		var result = Map.empty[String, Boolean]
		println("Groups =>" + groups.mkString(","))
		var flag = false
		groups.foreach(groupID => {
			val roles = DataHolder.group_roles.get(groupID).get._2
			var billing = roles.flatMap(roleId => DataHolder.roles.get(roleId).get.get(Constants.BILLING_PAYMENT_METHOD).get).distinct.map(_.toLowerCase())
			var bundle = roles.flatMap(roleId => DataHolder.roles.get(roleId).get.get(Constants.BUNDLE).get).distinct.map(_.toLowerCase())
			var payment = roles.flatMap(roleId => DataHolder.roles.get(roleId).get.get(Constants.PAYMENT).get).distinct.map(_.toLowerCase())
			actions.foreach(action => {
				if ((bundle.contains(Constants.VIEW) || bundle.contains(Constants.UPDATE)) &&
					(billing.contains(Constants.VIEW) || billing.contains(Constants.UPDATE))) {
					if (payment.contains(action))
						result += (action -> true)
				} else {
					if (payment.contains(action))
						result += (action -> false)
				}
			})
		})
		actions.foreach(action => {
			if (!result.contains(action)) {
				result += (action -> false)
			}
		})
		result
	}
	def evaluateRec(user: String, resourceID: String, resourceType: String, actions: Array[String]): Map[String, Boolean] = {
		var result = Map.empty[String, Boolean]
		val pgroups = MongoUtil.findGroupsRec(resourceID)
		if (pgroups.size == 0) {
			val ugroups = MongoUtil.findGroups(user)
			var result = Map.empty[String, Boolean]
			ugroups.foreach(groupID => {
				if (MongoUtil.isAdmin(user)) {
					actions.foreach(action => {
						if (!result.contains(action))
							result += (action -> true)
					})
					return result
				} else {
					actions.foreach(action => {
						if (!result.contains(action))
							result += (action -> false)
					})
				}
			}) //end of foreach
			result
		} else {
			val ugroups = MongoUtil.findGroups(user)
			val commonGroups = pgroups.intersect(ugroups)
			help(user, resourceType, actions, commonGroups)
		}
	}

}