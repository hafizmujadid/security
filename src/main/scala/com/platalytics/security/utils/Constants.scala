package com.platalytics.security.utils
import java.util.Properties
import java.io.InputStream
import java.io.FileInputStream
import javax.xml.parsers.DocumentBuilderFactory
import java.io.ByteArrayInputStream
import java.io.File
import java.util.logging.Logger
import java.util.logging.Level
/*
 This class stores configurations to run security project
 */
object Constants {
	private lazy val logger = Logger.getLogger(this.getClass.getName)

	var MONGO_IP: String = "122.129.79.68"
	var MONGO_PORT: String = "27017"
	var MONGO_DB_NAME: String = "deployment"
	var MONGO_COLLECTION_NAME: String = "policies"
	var AUDITS_DB_NAME: String = "audits"
	var ROLES_COLLECTION_NAME: String = "roles"
	var GROUPS_COLLECTION_NAME: String = "groups"
	var USERS_COLLECTION_NAME: String = "users"
	var COMPANY_COLLECTION_NAME: String = "companydetails"
	var PROJECTS_COLLECTION_NAME: String = "projects"
	var SKU_COLLECTION_NAME: String = "skus"
	var SERVICES_COLLECTION_NAME: String = "services"
	var CLUSTERS_COLLECTION_NAME: String = "clusters"
	var SKU_PERMS = "create,update,view,delete,deploy".split(",")
	var CLUSTER_PERMS = "create,update,view,delete,start,stop".split(",")
	var AUDITS_SCHEMA = "date,time,action,response,user,resourceName,resourceType".split(",")

	/*
	 * Permissions
	 */
	val PAY = "pay"
	val CREATE = "create"
	val UPDATE = "update"
	val DELETE = "delete"
	val VIEW = "view"
	val START = "start"
	val STOP = "stop"
	val DEPLOY = "deploy"

	/*
	 * 
	 * Resources
	 */
	val PROJECT = "project"
	val SKU = "sku"
	val SERVICE = "service"
	val CLUSTER = "cluster"
	val TEAM = "team"
	val BUNDLE = "bundle"
	val PAYMENT = "payment"
	val BILLING_PAYMENT_METHOD = "billing_payment_method"
	val PAYMENT_LOGS = "payment_logs"

	val SEPARATOR = ','
	private val CONSTANTS_FILE_PATH = "/home/constants.properties";
	private var _configurationLoaded = false

	def configurationLoaded = _configurationLoaded
	def configurationLoaded_=(configurationLoaded: Boolean) = _configurationLoaded = configurationLoaded

	def loadConfiguration(): Unit = {
		if (!configurationLoaded) {

			var _properties = new Properties

			val inputStream: InputStream = new FileInputStream(Constants.CONSTANTS_FILE_PATH)

			_properties.loadFromXML(inputStream)

			/** Initialize Constants **/
			/*MYSQL_HOST = _properties.getProperty("MYSQL_HOST")
			MYSQL_PORT = _properties.getProperty("MYSQL_PORT")
			MYSQL_DB_NAME = _properties.getProperty("MYSQL_DB_NAME")
			MYSQL_USER = _properties.getProperty("MYSQL_USER")
			MYSQL_PASSWORD = _properties.getProperty("MYSQL_PASSWORD")*/
			MONGO_IP = _properties.getProperty("MONGO_IP")
			MONGO_PORT = _properties.getProperty("MONGO_PORT")
			MONGO_DB_NAME = _properties.getProperty("MONGO_DB_NAME")
			MONGO_COLLECTION_NAME = _properties.getProperty("MONGO_COLLECTION_NAME")
			AUDITS_DB_NAME = _properties.getProperty("AUDITS_DB_NAME")
			ROLES_COLLECTION_NAME = _properties.getProperty("ROLES_COLLECTION_NAME")
			GROUPS_COLLECTION_NAME = _properties.getProperty("GROUPS_COLLECTION_NAME")
			USERS_COLLECTION_NAME = _properties.getProperty("USERS_COLLECTION_NAME")
			PROJECTS_COLLECTION_NAME = _properties.getProperty("PROJECTS_COLLECTION_NAME")

			logger.log(Level.INFO, "*****************************************************")
			logger.log(Level.INFO, "MONGO_IP : " + MONGO_IP)
			logger.log(Level.INFO, "MONGO_PORT : " + MONGO_PORT)
			logger.log(Level.INFO, "MONGO_DB_NAME : " + MONGO_DB_NAME)
			logger.log(Level.INFO, "MONGO_COLLECTION_NAME : " + MONGO_COLLECTION_NAME)
			logger.log(Level.INFO, "AUDITS_DB_NAME : " + AUDITS_DB_NAME)
			logger.log(Level.INFO, "ROLES_COLLECTION_NAME : " + ROLES_COLLECTION_NAME)
			logger.log(Level.INFO, "GROUPS_COLLECTION_NAME : " + GROUPS_COLLECTION_NAME)
			logger.log(Level.INFO, "USERS_COLLECTION_NAME : " + USERS_COLLECTION_NAME)
			logger.log(Level.INFO, "PROJECTS_COLLECTION_NAME : " + PROJECTS_COLLECTION_NAME)
			logger.log(Level.INFO, "*****************************************************")
			configurationLoaded = true
		}
	}

}