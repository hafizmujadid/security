package com.platalytics.security.utils
import com.google.gson.Gson
import play.api.libs.json.Json
import play.api.libs.json.JsArray
import com.platalytics.security.core.Group
import com.platalytics.security.core.Permissions
import com.platalytics.security.core.Policy
import com.platalytics.security.core.Role
import scala.Array.canBuildFrom
import com.platalytics.security.auditing.AuditDTO

object Util {
  def toJson(policy: Policy): String = {
    val gson = new Gson()
    gson.toJson(policy)
  }

  def toJsonAudit(audit: AuditDTO): String = {
    val gson = new Gson()
    gson.toJson(audit)
  }

  def jsonToPolicy(json: String): Policy = {
    val policy = new Policy()
    val jsonString = Json.parse(json)
    policy.id = ((jsonString \ "_id").as[Map[String, String]]).get("$oid").get
    policy.r_id = (jsonString \ "resource_id").as[String]
    policy.r_name = (jsonString \ "name").as[String]
    policy.r_type = (jsonString \ "resource").as[String]
    policy.is_enabled = (jsonString \ "is_enabled").as[Boolean]
    val arr = (jsonString \ "groups").asInstanceOf[JsArray]
    var result = ""
    arr.value.map(elem => {
      result += ((elem \ "$oid").as[String]) + ","
    })
    policy.groups = result.dropRight(1).split(",")
    policy
  }
  def jsonToRole(json: String): Role = {
    val jsonString = Json.parse(json)
    var role = new Role
    var permission = new Permissions
    role._id = ((jsonString \ "_id").as[Map[String, String]]).get("$oid").get
    val map = (jsonString \ "permissions").as[Map[String, Seq[String]]]
    permission.project = map.get("project").getOrElse(Seq()).toArray
    permission.sku = map.get("sku").getOrElse(Seq()).toArray
    permission.cluster = map.get("cluster").getOrElse(Seq()).toArray
    permission.services = map.get("services").getOrElse(Seq()).toArray
    permission.billing_payment_method = map.get("billing_payment_method").getOrElse(Seq()).toArray
    permission.bundle = map.get("bundle").getOrElse(Seq()).toArray
    permission.team = map.get("team").getOrElse(Seq()).toArray
    permission.payment = map.get("payment").getOrElse(Seq()).toArray
    permission.payment_logs = map.get("payment_logs").getOrElse(Seq()).toArray
    role.permissions = permission
    role
  }
  def jsonToGroup(json: String): Group = {
    val jsonString = Json.parse(json)
    var group = new Group
    var permission = new Permissions
    group._id = ((jsonString \ "_id").as[Map[String, String]]).get("$oid").get
    val map = (jsonString \ "roles").as[Array[Map[String, String]]]
    val roles = map.map(x => x.get("$oid").get)
    group.roles = roles
    group.name = (jsonString \ "name").as[String]
    group
  }
  def getJsonStringForSchema(): String = {
    val columns = Constants.AUDITS_SCHEMA
    val schema = (0 until columns.length).map { i =>
      val columnName = columns(i)
      Map(("field" -> Json.toJson(columnName.trim)),
        ("type" -> Json.toJson("string")))
    }
    Json.stringify(Json.toJson(Map("schema" -> schema)))
  }

  def getJsonStringForData(data: String, schema: Array[String], splitter: Char): String = {
  	if(data==null){
  		val jsonData= Array[scala.collection.immutable.Map[String,play.api.libs.json.JsValue]]()
  		return Json.stringify(Json.toJson(Map("data" -> jsonData)))
  	}
    val records = data.split("\n").map(_.split(","))
    val jsonData = records.map(line => {
      (0 until schema.size).map(i => {
        if (i < line.size) {
          (schema(i), Json.toJson(line(i)))
        } else
          (schema(i), Json.toJson("null"))
      }).toMap
    })
    if (records.size == 0) {
      val res = Json.toJson("[]")
      Json.stringify(Json.toJson(Map("data" -> res)))
    }
    else
      Json.stringify(Json.toJson(Map("data" -> jsonData)))
  }
  def theMonth(month: Int): String = {
    val monthNames = Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
    monthNames(month - 1)
  }
}