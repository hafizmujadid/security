package com.platalytics.security.utils

object DataHolder {
	var roles: Map[String, Map[String, Array[String]]] = Map.empty
	//(group-id=>groupname,roles)
	var group_roles: Map[String, (String, Array[String])] = Map.empty
	def load(company: String): String = {
		roles.empty
		group_roles.empty
		val allRoles = MongoUtil.loadAllRoles(company)

		allRoles.foreach(role => {
			var map: Map[String, Array[String]] = Map.empty
			map += (Constants.PROJECT -> role.permissions.project)
			map += (Constants.SKU -> role.permissions.sku)
			map += (Constants.CLUSTER -> role.permissions.cluster)
			map += (Constants.SERVICE -> role.permissions.services)
			map += (Constants.TEAM -> role.permissions.team)
			map += (Constants.BUNDLE -> role.permissions.bundle)
			map += (Constants.BILLING_PAYMENT_METHOD -> role.permissions.billing_payment_method)
			map += (Constants.PAYMENT -> role.permissions.payment)
			map += (Constants.PAYMENT_LOGS -> role.permissions.payment_logs)
			roles += (role._id -> map)
		})
		val allGroups = MongoUtil.loadAllGroups(company)
		allGroups.foreach(group => {
			group_roles += (group._id -> (group.name, group.roles))
		})
		return "Roles & Groups are initiated successfully!"
	}

}