package com.platalytics.security.utils

import com.mongodb.DBObject
import com.mongodb.util.JSON
import com.mongodb.MongoClient
import com.mongodb.DB
import com.mongodb.DBCollection
import com.mongodb.WriteConcern
import com.mongodb.DBCursor
import com.mongodb.BasicDBObject
import java.util.regex.Pattern
import play.api.libs.json.Json
import play.api.libs.json.JsArray
import com.platalytics.security.core.Group
import com.platalytics.security.core.Policy
import com.platalytics.security.core.Role
import scala.Array.canBuildFrom
import com.platalytics.security.core.PolicyEvaluator
import com.platalytics.security.auditing.AuditDTO
import com.google.gson.Gson
import play.api.libs.json.JsValue
import com.mongodb.client.model.Projections
import org.bson.types.ObjectId
/*
 This class handles all interaction with mongodb including , read policies, write
 and search on different criteria.
 */
object MongoUtil {
	var mongoClient: MongoClient = null
	var db: DB = null
	private def connect() {
		mongoClient = new MongoClient(Constants.MONGO_IP, Constants.MONGO_PORT.toInt)
		mongoClient.setWriteConcern(WriteConcern.JOURNALED)
	}
	def getDB(): DB = {
		if (db == null)
			db = mongoClient.getDB(Constants.MONGO_DB_NAME)
		return db
	}
	def getCollection(): DBCollection = {
		connect
		val db = getDB
		db.getCollection(Constants.MONGO_COLLECTION_NAME)
	}

	def getDB(name: String): DB = {
		if (db == null)
			db = mongoClient.getDB(name)
		return db
	}
	def getCollection(dbName: String, collection: String): DBCollection = {
		connect
		val db = getDB(dbName)
		db.getCollection(collection)
	}

	def list(start: Int, limit: Int): String = {
		val gson = new Gson
		val cursor = getCollection(Constants.MONGO_DB_NAME, Constants.AUDITS_DB_NAME).find.sort(new BasicDBObject("datetime", -1)).skip(start).limit(limit)
		if (cursor.size() == 0)
			return (Util.getJsonStringForSchema +
				Util.getJsonStringForData(null, Constants.AUDITS_SCHEMA, Constants.SEPARATOR)).
				replace("}{", ",")

		var result = new StringBuilder
		while (cursor.hasNext()) {
			val obj = cursor.next()
			result.append(obj.get("date") + ",")
			result.append(obj.get("time") + ",")
			result.append(obj.get("action") + ",")
			result.append(obj.get("response") + ",")
			result.append(obj.get("user") + ",")
			result.append(obj.get("resourceName") + ",")
			result.append(obj.get("resourceType"))
			result.append("\n")
		}
		result = result.dropRight(1)
		cursor.close()
		return (Util.getJsonStringForSchema +
			Util.getJsonStringForData(result.toString, Constants.AUDITS_SCHEMA, Constants.SEPARATOR)).
			replace("}{", ",")
	}

	def getCollection(collection: String): DBCollection = {
		connect
		getDB.getCollection(collection)
	}

	def write(json: String, collectionName: String): Boolean = {
		try {
			val dbObject = JSON.parse(json).asInstanceOf[DBObject]
			getCollection(collectionName).save(dbObject)
			true
		} catch {
			case ex: Exception => false
		}
	}
	def close() = {
		mongoClient.close()
	}
	def upsert(resourceName: String, json: String): Boolean = {
		try {
			val query = new BasicDBObject("resource", resourceName)
			val dbObject = JSON.parse(json).asInstanceOf[DBObject]
			getCollection().update(
				query,
				new BasicDBObject("$set", dbObject), true, false)
			true
		} catch {
			case ex: Exception => false
		}
	}
	def getPolicies(id: String, resType: String): Array[Policy] = {
		val query = new BasicDBObject("resource_id", Pattern.compile(id, Pattern.CASE_INSENSITIVE)).append("resource", Pattern.compile(resType, Pattern.CASE_INSENSITIVE))
		val result = getCollection(Constants.MONGO_DB_NAME, Constants.MONGO_COLLECTION_NAME).find(query)
		val res = toPolicies(result)
		result.close()
		res
	}
	def toPolicies(cursor: DBCursor) = {
		var list = new Array[Policy](cursor.size())
		var i = 0
		while (cursor.hasNext()) {
			list(i) = Util.jsonToPolicy(cursor.next().toString())
			i += 1
		}
		cursor.close()
		list
	}
	def loadAllRoles(company: String): Array[Role] = {
		val query = new BasicDBObject("company", Pattern.compile(company, Pattern.CASE_INSENSITIVE))
		val cursor = getCollection(Constants.MONGO_DB_NAME, Constants.ROLES_COLLECTION_NAME).find(query)
		val res = toRoles(cursor)
		cursor.close()
		res
	}
	def toRoles(cursor: DBCursor) = {
		var list = new Array[Role](cursor.size())
		var i = 0
		while (cursor.hasNext()) {
			list(i) = Util.jsonToRole(cursor.next().toString())
			i += 1
		}
		cursor.close()
		list
	}

	def loadAllGroups(company: String): Array[Group] = {
		val query = new BasicDBObject("company", Pattern.compile(company, Pattern.CASE_INSENSITIVE))
		val cursor = getCollection(Constants.MONGO_DB_NAME, Constants.GROUPS_COLLECTION_NAME).find(query)
		val res = toGroups(cursor)
		cursor.close()
		res
	}
	def toGroups(cursor: DBCursor) = {
		var list = new Array[Group](cursor.size())
		var i = 0
		while (cursor.hasNext()) {
			list(i) = Util.jsonToGroup(cursor.next().toString())
			i += 1
		}
		cursor.close()
		list
	}
	def findGroups(user: String): Array[String] = {
		val query = new BasicDBObject("username", user)
		val obj = getCollection(Constants.MONGO_DB_NAME, Constants.USERS_COLLECTION_NAME).findOne(query)
		if (obj == null)
			return Array()
		else {
			val jsonString = Json.parse(obj.toString())
			val arr = (jsonString \ "groups").asInstanceOf[JsArray]
			var result = ""
			arr.value.map(elem => {
				result += ((elem \ "$oid").as[String]) + ","
			})
			result.dropRight(1).split(",")
		}
	}
	def findGroupByUserId(user: String): Array[String] = {
		val query = new BasicDBObject("_id", new ObjectId(user))
		val fields = new BasicDBObject("groups", "1")
		val obj = getCollection(Constants.MONGO_DB_NAME, Constants.USERS_COLLECTION_NAME).findOne(query, fields)
		if (obj == null)
			return Array()
		else {
			val jsonString = Json.parse(obj.toString())
			val arr = (jsonString \ "groups").asInstanceOf[JsArray]
			var result = ""
			arr.value.map(elem => {
				result += ((elem \ "$oid").as[String]) + ","
			})
			result.dropRight(1).split(",")
		}
	}

	def findGroups(id: String, resType: String): Array[String] = {
		val query = new BasicDBObject("resource_id", Pattern.compile(id, Pattern.CASE_INSENSITIVE))
			.append("resource", Pattern.compile(resType, Pattern.CASE_INSENSITIVE))
			.append("is_enabled", true)
		val fields = new BasicDBObject("groups", "1")
		val obj = getCollection(Constants.MONGO_DB_NAME, Constants.MONGO_COLLECTION_NAME).findOne(query, fields)
		if (obj != null) {
			val jsonString = Json.parse(obj.toString())
			val arr = (jsonString \ "groups").asInstanceOf[JsArray]
			var result = ""
			arr.value.map(elem => {
				result += ((elem \ "$oid").as[String]) + ","
			})
			result.dropRight(1).split(",")
		} else
			Array()
	}

	def listResource(resourceType: String, user: String): Array[String] = {
		val ugroups = findGroups(user)
		println("UGroups => " + ugroups.mkString(","))
		if (isAdmin(user)) {
			println("******** Admin User *********")
			val admin = resourceHelper(ugroups, resourceType)
			val normal = normaluserEvaluator(resourceType, user, ugroups)
			admin.union(normal).distinct
		} else {
			println("******* Normal User **********")
			return normaluserEvaluator(resourceType, user, ugroups)
		}

	}

	def getProjectID(clusterID: String): String = {
		var query = new BasicDBObject("cluster", Pattern.compile(clusterID, Pattern.CASE_INSENSITIVE))
		var obj = getCollection(Constants.MONGO_DB_NAME, Constants.PROJECTS_COLLECTION_NAME).findOne(query)
		if (obj != null) {
			var jsonString = Json.parse(obj.toString())
			return ((jsonString \ "_id").as[Map[String, String]]).get("$oid").get
		} else
			return null
	}
	def getProjectIDforSku(skuID: String): String = {
		val query = new BasicDBObject("sku._id", Pattern.compile(skuID, Pattern.CASE_INSENSITIVE))
		var obj = getCollection(Constants.MONGO_DB_NAME, Constants.PROJECTS_COLLECTION_NAME).findOne(query)
		if (obj != null) {
			var jsonString = Json.parse(obj.toString())
			return ((jsonString \ "_id").as[Map[String, String]]).get("$oid").get
		} else
			return null
	}
	def findGroupsRec(clusterID: String): Array[String] = {
		val projectID = getProjectID(clusterID)
		if (projectID == null) {
			val cg = findGroups(clusterID, "cluster")
			if (cg == null) Array() else cg

		} else {
			val pg = findGroups(projectID, "project")
			val cg = findGroups(clusterID, "cluster")
			if (cg != null && pg != null)
				cg.union(pg)
			else if (cg == null && pg == null)
				Array()
			else {
				if (cg == null && pg != null)
					pg
				else if (pg == null && cg != null)
					cg
				else
					Array()
			}
		}
	}

	def getUserCompany(user: String): String = {
		val query = new BasicDBObject("username", Pattern.compile(user, Pattern.CASE_INSENSITIVE))
		var obj = getCollection(Constants.MONGO_DB_NAME, Constants.USERS_COLLECTION_NAME).findOne(query)
		if (obj != null) {
			var jsonString = Json.parse(obj.toString())
			return ((jsonString \ "companyDetails").as[Map[String, String]]).get("$oid").get
		} else
			return null
	}
	def idToName(id: String): String = {
		val query = new BasicDBObject("_id", new ObjectId(id))
		var obj = getCollection(Constants.MONGO_DB_NAME, Constants.COMPANY_COLLECTION_NAME).findOne(query)
		if (obj != null) {
			var jsonString = Json.parse(obj.toString())
			return (jsonString \ "company").as[String]
		} else
			return null
	}
	def isAdmin(user: String): Boolean = {
		val groups = findGroups(user)
		groups.foreach { x =>
			val query = new BasicDBObject("_id", new ObjectId(x))
			var obj = getCollection(Constants.MONGO_DB_NAME, Constants.GROUPS_COLLECTION_NAME).findOne(query)
			if (obj != null) {
				var jsonString = Json.parse(obj.toString())
				val groupname = ((jsonString \ "name").as[String]).toLowerCase()
				if (groupname.contains("admin"))
					return true
			} else
				return false
		}
		return false
	}

	def findAdminID(): String = {

		val query = new BasicDBObject("title", Pattern.compile("", Pattern.CASE_INSENSITIVE))
		var obj = getCollection(Constants.MONGO_DB_NAME, Constants.ROLES_COLLECTION_NAME).findOne(query)
		if (obj != null) {
			var jsonString = Json.parse(obj.toString())
			return ((jsonString \ "_id").as[Map[String, String]]).get("$oid").get
		} else
			return null
	}

	def resourceHelper(ugroups: Array[String], resourceType: String): Array[String] = {
		val arr = getAllResources(resourceType)
		var result = new StringBuilder
		arr.foreach(elem => {
			val groups = findGroupByUserId(elem._2)
			if (groups.intersect(ugroups).length > 0)
				result.append(elem._1 + ",")
		})
		result.split(',').filter(x => x.length() > 0)
	}

	def normaluserEvaluator(resourceType: String, user: String, ugroups: Array[String]): Array[String] = {
		val query = new BasicDBObject("resource", Pattern.compile(resourceType, Pattern.CASE_INSENSITIVE))
			.append("is_enabled", true)
		val obj = getCollection(Constants.MONGO_DB_NAME, Constants.MONGO_COLLECTION_NAME).find(query)
		var res = toPolicies(obj)
		println("Policies => " + res.length)
		var map = Map.empty[String, Array[String]]

		res.foreach { p =>
			{
				if (resourceType.equalsIgnoreCase("cluster")) {
					var result = PolicyEvaluator.evaluateRec(user, p.r_id, resourceType, Constants.CLUSTER_PERMS)
					val perms = result.filter(x => x._2).keys.toArray
					if (perms.size > 0) {
						map += (p.r_id -> perms)
					}
				} else {
					val pgroups = p.getGroups
					println("pGroups => " + pgroups.mkString(","))
					val commonGroups = pgroups.intersect(ugroups)
					println("Common Groups =>" + commonGroups.mkString(","))
					if (commonGroups.length > 0) {
						var perms = PolicyEvaluator.findPerms(resourceType, commonGroups)
						if (perms.size > 0) {
							map += (p.r_id -> perms) //.filter(x => x.equalsIgnoreCase("update") || x.equalsIgnoreCase("delete"))
						}
					}
				}
			}
		}
		return map.keys.toArray
	}
	def getAllResources(resourceType: String): Array[(String, String)] = {
		resourceType match {
			case Constants.PROJECT => getAllProjects
			case Constants.SKU => getAllSkus
			case Constants.SERVICE => getAllServices
			case Constants.CLUSTER => getAllClusters
		}
	}
	def getAllProjects(): Array[(String, String)] = {
		val result = getCollection(Constants.MONGO_DB_NAME, Constants.PROJECTS_COLLECTION_NAME).find
		convert(result)
	}
	def getAllSkus(): Array[(String, String)] = {
		val result = getCollection(Constants.MONGO_DB_NAME, Constants.SKU_COLLECTION_NAME).find
		convert(result)
	}
	def getAllServices(): Array[(String, String)] = {
		val result = getCollection(Constants.MONGO_DB_NAME, Constants.SERVICES_COLLECTION_NAME).find
		convert(result)
	}
	def getAllClusters(): Array[(String, String)] = {
		val result = getCollection(Constants.MONGO_DB_NAME, Constants.CLUSTERS_COLLECTION_NAME).find
		convert(result)
	}
	def convert(result: DBCursor): Array[(String, String)] = {
		var arr: Array[(String, String)] = new Array[(String, String)](result.size())
		var i = 0
		while (result.hasNext()) {
			var jsonString = Json.parse(result.next().toString())
			val id = ((jsonString \ "_id").as[Map[String, String]]).get("$oid").get
			val user = (jsonString \ "user_id").as[String]
			arr(i) = (id, user)
			i += 1

		}
		result.close()
		return arr
	}
}