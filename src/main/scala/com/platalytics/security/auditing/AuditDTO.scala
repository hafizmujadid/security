package com.platalytics.security.auditing

import scala.beans.BeanProperty
/*
 This class is to store one access audit
 Who accessed what and when. what was the response
 */
class AuditDTO {
	@BeanProperty var datetime: Long = _
	@BeanProperty var date: String = _
	@BeanProperty var time: String = _
	@BeanProperty var action: String = _
	@BeanProperty var response: String = _
	@BeanProperty var user: String = _
	@BeanProperty var resourceName: String = _
	@BeanProperty var resourceType: String = _
}