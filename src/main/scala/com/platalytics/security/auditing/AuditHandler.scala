package com.platalytics.security.auditing

import com.platalytics.security.utils._
import java.util.Date
/*
 This class is to be implemented. It will store and fetch audits to/from underlying system.
 */
object AuditHandler {
	def audit(audit: AuditDTO): Boolean = {
		MongoUtil.write(Util.toJsonAudit(audit), Constants.AUDITS_DB_NAME)
		true
	}
}